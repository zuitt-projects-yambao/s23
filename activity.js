/*db.users.insertOne(
	{
		"firstName" : "Zidane",
		"lastName" : "Tribal",
		"email" : "thief@mail.com",
		"password" : "daggerThief",
		"isAdmin" : false
	}
)*/

// USERS Collection

db.users.insertMany([
	{
		"firstName" : "Zidane",
		"lastName" : "Tribal",
		"email" : "thief@mail.com",
		"password" : "daggerThief",
		"isAdmin" : false
	},
	{
		"firstName" : "Squall",
		"lastName" : "Leonhart",
		"email" : "leonhart@mail.com",
		"password" : "gunBlade",
		"isAdmin" : false
	},
	{
		"firstName" : "Cloud",
		"lastName" : "Strife",
		"email" : "cloudStrife@mail.com",
		"password" : "busterSword",
		"isAdmin" : false
	},
	{
		"firstName" : "Noctis",
		"lastName" : "Lucis",
		"email" : "Caelum@mail.com",
		"password" : "TooManyWeapon",
		"isAdmin" : false
	},
	{
		"firstName" : "Clive",
		"lastName" : "Rosfield",
		"email" : "ffxvi@mail.com",
		"password" : "aftermanyYears@mail.com",
		"isAdmin" : false
	}])



// COURSES Collection

db.courses.insertMany([
{
	"name" : "Farming",
	"price": 1500,
	"isActive" : false
},
{
	"name" : "Kill Monster",
	"price": 2500,
	"isActive" : false
},
{
	"name" : "Defeat Boss",
	"price": 5000,
	"isActive" : false
}

])

// Finding all regular/non-admin users

db.users.find();

// Updating the first user in collection as an admin

db.users.updateOne({}, {$set : {"isAdmin" : true}})

// Update one of the courses as active
// This will depend on the selected criteria
db.courses.updateOne(
	{"name" : "Defeat Boss"},
	{$set : {"isActive" : true}})

// Deleting all inactive courses or in false
db.courses.deleteMany({"isActive" : false})